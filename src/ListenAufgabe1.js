import React,{useState} from "react";

export default function ListExample1() {
    const [colors, setColors] = useState(['red', 'green', 'blue']);
    return (
        <div className="container">
            <h1>List Example 1</h1>
            <ul>
                { colors.map((color,idx) => <li key={'color-'+idx}>{color}</li>) }
            </ul>
        </div>
    );
}
