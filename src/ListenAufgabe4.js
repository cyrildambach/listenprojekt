import React,{useState} from "react";

export default function ListExample4() {
    const data = [
        { title: 'red', red: 255, green: 0, blue: 0 },
        { title: 'green', red: 0, green: 255, blue: 0 },
        { title: 'blue', red: 0, green: 0, blue: 255 }
    ]
    const [colors, setColors] = useState(data);
    const [titleinput, setTitleInput] = useState(data);

    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length === 1 ? "0" + hex : hex;
    }
    function rgbToHex(r, g, b) {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }
    function myButtonHandler(event){
      setColors([ ...colors, { title: titleinput, red:Math.floor(Math.random() * 255), green:Math.floor(Math.random() * 255), blue:Math.floor(Math.random() * 255)} ])

    }
    return (
        <div className="container">
            <h1>List Example 4</h1>
            <ul>
                { colors.map((color,idx) => <li style={{color: rgbToHex(color.red,color. green,color.blue)}} key={'color-'+idx}>{color.title} {rgbToHex(color.red,color. green,color.blue)} </li>) }
            </ul>
            <input type={"text"} onChange={e => setTitleInput(e.target.value)}></input><button onClick = { (e) => myButtonHandler(e) }>random color</button>

        </div>
    );
}
