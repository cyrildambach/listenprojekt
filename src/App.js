import './App.css';
import ListExample1 from "./ListenAufgabe1";
import ListExample2 from "./ListenAufgabe2";
import ListExample3 from "./ListenAufgabe3";
import ListExample4 from "./ListenAufgabe4";

function App() {
  return (
    <div>
        <ListExample1/>
        <ListExample2/>
        <ListExample3/>
        <ListExample4/>
    </div>
  );
}

export default App;
