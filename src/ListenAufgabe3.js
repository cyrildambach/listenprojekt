import React,{useState} from "react";

export default function ListExample3() {
    const data = [
        { title: 'red', red: 255, green: 0, blue: 0 , html: '#FF0000'},
        { title: 'green', red: 0, green: 255, blue: 0 , html: '#00FF00'},
        { title: 'blue', red: 0, green: 0, blue: 255 , html: '#0000FF'}
    ]
    const [colors, setColors] = useState(data);
    return (
        <div className="container">
            <h1>List Example 3</h1>
            <ul>
                { colors.map((color,idx) => <li style={{color: color.html}} key={'color-'+idx}>{color.title} {color.html}</li>) }
            </ul>
        </div>
    );
}
